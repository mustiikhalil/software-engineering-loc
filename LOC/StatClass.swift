//
//  main.swift
//  LOC
//
//  Created by Mustafa Khalil on 5/15/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import Foundation


class StatClass{
    
    private var _path: String
    
    var path : String {
        get{
            return _path
        }
        set{
            _path = newValue
        }
    }
    
    init(Path: String) {
       _path = Path
    }
    
    func countMethods() -> Int{
        let fileContent = try? NSString(contentsOfFile: _path, encoding: .init())
        
        let y = fileContent?.components(separatedBy: "\n")
        
        var methodCount = 0
        print(fileContent)
        
        for line in y!{
            
            if ((line.contains("func") && line.contains("{")) || (line.contains("get") && line.contains("{"))
                || (line.contains("set") && line.contains("{")) || (line.contains("init") && line.contains("{"))){
                methodCount += 1
            }
            else{
                continue
            }
        }
        return methodCount
    }
    
//    
//    func countMethods() -> Int{
//        let fileContent = try? NSString(contentsOfFile: _path, encoding: .init())
//        
//        let y = fileContent?.components(separatedBy: "\n")
//        
//        var methodCount = 0
//        print(fileContent)
//        
//        for line in y!{
//            
//            if ((line.contains("func") && line.contains("{")) || (line.contains("get") && line.contains("{"))
//                || (line.contains("set") && line.contains("{")) || (line.contains("init") && line.contains("{"))){
//                methodCount += 1
//            }
//            else{
//                continue
//            }
//        }
//        return methodCount
//    }

}

